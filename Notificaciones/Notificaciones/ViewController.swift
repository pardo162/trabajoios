//
//  ViewController.swift
//  Notificaciones
//
//  Created by alejo on 10/1/18.
//  Copyright © 2018 alejo. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {

    @IBOutlet weak var entrada: UITextField!
    @IBOutlet weak var salida: UILabel!
    
    var notificacionMss = "El texto es "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Para recuperar
        //instanciar UserDefaults
        let defaulults = UserDefaults.standard
        //acceder al valor por medio del key
        entrada.text = defaulults.object(forKey: "label") as? String
        
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted,error) in}

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    

    }
    @IBAction func clickbtn(_ sender: Any) {
        salida.text = entrada.text
        //Para guardar
        //instanciar UserDefaults
        let defaulults = UserDefaults.standard
        
        //guardae cariables en defaults Int, Bool, Double, String,
        
        defaulults.set(entrada.text, forKey: "label")
        sendNotificaction()
        
    }
    
    func sendNotificaction(){
        notificacionMss += salida.text!
        // autorizar request ( está en DidLoad)
        // crear contenido de notificacion
        
        let content = UNMutableNotificationContent()
        content.title = "Notificacion Titel"
        content.subtitle = "Notifiacacion Subtitle"
        content.body = notificacionMss
        
        //creqr acciones
        
        let repeatAction = UNNotificationAction(identifier: "repeat" , title: "Repetir", options: [])
        let changeMessageAction = UNTextInputNotificationAction(identifier: "change", title: "Cambiar mensaje", options: [])
        
        // agregar accion a un UNNotificationCategory
        
        let category = UNNotificationCategory(identifier: "actionscat", actions: [repeatAction,changeMessageAction], intentIdentifiers: [], options: [])
        
        // agregar el category al content
        
        content.categoryIdentifier = "actionscat"
        
        //agregar categoria al UNUserNotificationCenter
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        // definer un trigger
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        //definir un identificador para la notificacion
        
        let identifire = "Notificacion"
        
        //crear un request
        
        let notificationRequest = UNNotificationRequest(identifier: identifire, content: content, trigger: trigger)
        
        //añade el request al UNUserNotificationCenter
        
        UNUserNotificationCenter.current().add(notificationRequest){(error) in }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.actionIdentifier{
        case "repeat":
            self.sendNotificaction()
            break
        case "change":
            let txtResponse = response as! UNTextInputNotificationResponse
            notificacionMss += txtResponse.userText
            self.sendNotificaction()
            completionHandler()
            break
        default:
            break
            
        }
        
    }
}

